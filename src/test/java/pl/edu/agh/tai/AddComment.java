package pl.edu.agh.tai;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import pl.edu.agh.tai.Application;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

import javax.validation.constraints.AssertTrue;

import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration


public class AddComment {
    private WebDriver driver;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();


    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        baseUrl = "http://localhost:8080/";
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    @Test
    public void testAddComment() throws Exception {
        driver.get(baseUrl + "/events");
        driver.findElement(By.cssSelector("button.btn.btn-danger")).click();
        driver.findElement(By.linkText("Connect with Facebook")).click();
        driver.findElement(By.linkText("EventTest #2")).click();
        driver.findElement(By.cssSelector("div.group-container > div.new-msg-form-container > form > #content")).clear();
        Dimension size1 = driver.findElement(By.cssSelector("div.group-container")).getSize();
        driver.findElement(By.cssSelector("div.group-container > div.new-msg-form-container > form > #content")).sendKeys("testowy komentarz");
        driver.findElement(By.xpath("(//input[@value='Add'])[2]")).click();
        Dimension size2 = driver.findElement(By.cssSelector("div.group-container")).getSize();
        assertTrue(size1.getHeight()<size2.getHeight());
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }

}


