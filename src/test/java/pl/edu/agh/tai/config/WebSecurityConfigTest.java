package pl.edu.agh.tai.config;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configurers.provisioning.JdbcUserDetailsManagerConfigurer;

import javax.sql.DataSource;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class WebSecurityConfigTest {

    @Mock
    private DataSource dataSource;

    @Test
    public void configureGlobal() throws Exception {
        AuthenticationManagerBuilder authBuilder = mock(AuthenticationManagerBuilder.class);
        JdbcUserDetailsManagerConfigurer jdbcConfigurer = mock(JdbcUserDetailsManagerConfigurer.class);
        when(authBuilder.jdbcAuthentication()).thenReturn(jdbcConfigurer);
        when(jdbcConfigurer.dataSource(any(DataSource.class))).thenReturn(jdbcConfigurer);

        WebSecurityConfig config = new WebSecurityConfig();
        config.setDataSource(dataSource);
        config.configureGlobal(authBuilder);

        verify(authBuilder, times(1)).jdbcAuthentication();
        verify(jdbcConfigurer, times(1)).dataSource(dataSource);
        verify(jdbcConfigurer, times(1)).withDefaultSchema();
    }
}