package pl.edu.agh.tai.config;


import org.junit.Before;
import org.junit.Test;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;

import static org.mockito.Mockito.*;

public class MvcConfigTest {

    private ViewControllerRegistry registry;
    private ViewControllerRegistration registration;
    private MvcConfig mvcConfig = new MvcConfig();

    @Before
    public void before() {
        registry = mock(ViewControllerRegistry.class);
        registration = mock(ViewControllerRegistration.class);
        when(registry.addViewController(anyString())).thenReturn(registration);

    }

    @Test
    public void addsEventsViewController() {
        mvcConfig.addViewControllers(registry);

        verify(registry, times(1)).addViewController("/events");
        verify(registration, times(1)).setViewName("home");
    }

    @Test
    public void addsLoginViewController() {
        mvcConfig.addViewControllers(registry);

        verify(registry, times(1)).addViewController("/login");
        verify(registration, times(1)).setViewName("login");
    }

    @Test
    public void addsRootRedirectViewController() {
        mvcConfig.addViewControllers(registry);

        verify(registry, times(1)).addRedirectViewController("/", "/events");
    }

    @Test
    public void addsFacebookRedirectViewController() {
        mvcConfig.addViewControllers(registry);

        verify(registry, times(1)).addRedirectViewController("/authenticate/facebook", "/auth/facebook?scope=email,user_events,rsvp_event,publish_actions");
    }

}