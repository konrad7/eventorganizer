package pl.edu.agh.tai.config;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.social.UserIdSource;
import org.springframework.social.connect.ConnectionFactoryLocator;
import org.springframework.social.connect.UsersConnectionRepository;
import org.springframework.social.connect.jdbc.JdbcUsersConnectionRepository;
import org.springframework.social.security.AuthenticationNameUserIdSource;
import pl.edu.agh.tai.dao.UsersDao;

import javax.sql.DataSource;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class SocialConfigTest {

    @Mock
    private DataSource dataSource;

    @Mock
    private UsersDao usersDao;

    @Test
    public void returnsUserIdSource() {
        SocialConfig socialConfig = new SocialConfig();
        UserIdSource userIdSource = socialConfig.getUserIdSource();

        assertNotNull(userIdSource);
        assertTrue(userIdSource instanceof AuthenticationNameUserIdSource);
    }

    @Test
    public void returnsUsersConnectionRepository() throws Exception {
        ConnectionFactoryLocator connectionFactoryLocator = mock(ConnectionFactoryLocator.class);
        SocialConfig socialConfig = new SocialConfig();
        socialConfig.setDataSource(dataSource);
        UsersConnectionRepository repository = socialConfig.getUsersConnectionRepository(connectionFactoryLocator);

        assertNotNull(repository);
        assertTrue(repository instanceof JdbcUsersConnectionRepository);
    }

}