package pl.edu.agh.tai.services;


import org.junit.Test;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.UserProfile;
import pl.edu.agh.tai.dao.UsersDao;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class AccountConnectionSignUpServiceTest {

    @Test
    public void executeConnection() {
        UsersDao usersDao = mock(UsersDao.class);
        AccountConnectionSignUpService service = new AccountConnectionSignUpService(usersDao);
        Connection connection = mock(Connection.class);
        UserProfile userProfile = mock(UserProfile.class);
        when(connection.fetchUserProfile()).thenReturn(userProfile);
        when(userProfile.getEmail()).thenReturn("aaa@bbb.com");

        String email = service.execute(connection);

        verify(usersDao, times(1)).createUser("aaa@bbb.com");
        assertEquals("aaa@bbb.com", email);
    }
}