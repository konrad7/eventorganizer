package pl.edu.agh.tai.services;


import org.junit.Test;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.social.security.SocialUserDetails;

import java.util.Collections;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SimpleSocialUsersDetailServiceTest {

    @Test
    public void loadUserByUserId() {
        UserDetailsService userService = mock(UserDetailsService.class);
        UserDetails userDetails = mock(UserDetails.class);
        when(userDetails.getUsername()).thenReturn("username");
        when(userDetails.getPassword()).thenReturn("password");
        when(userDetails.getAuthorities()).thenReturn(Collections.EMPTY_LIST);
        when(userService.loadUserByUsername("123")).thenReturn(userDetails);
        SimpleSocialUsersDetailService socialService = new SimpleSocialUsersDetailService(userService);

        SocialUserDetails socialUser = socialService.loadUserByUserId("123");

        assertNotNull(socialUser);
        assertEquals("username", socialUser.getUsername());
        assertEquals("password", socialUser.getPassword());
        assertTrue(socialUser.getAuthorities().isEmpty());
    }
}