package pl.edu.agh.tai.controllers;


import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.*;
import org.springframework.ui.ExtendedModelMap;
import org.springframework.ui.Model;
import pl.edu.agh.tai.domain.FbPost;
import pl.edu.agh.tai.domain.NewMessage;
import pl.edu.agh.tai.utils.FbGraphApi;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ FbGraphApi.class })
public class EventsControllerTest {

    @Mock
    private Facebook facebook;
    @Mock
    private ConnectionRepository connectionRepository;
    @Mock
    private Connection connection;
    @Mock
    private UserOperations userOperations;
    @Mock
    private User user;
    @Mock
    private EventOperations eventOperations;
    @Mock
    private FeedOperations feedOperations;
    @Mock
    private CommentOperations commentOperations;

    private Model model;
    private EventsController controller;

    @Before
    public void before() {
        model = new ExtendedModelMap();
        controller = new EventsController(facebook, connectionRepository);
        when(connectionRepository.findPrimaryConnection(Facebook.class)).thenReturn(connection);
        when(user.getName()).thenReturn("Tom");
        when(userOperations.getUserProfile()).thenReturn(user);
        when(facebook.userOperations()).thenReturn(userOperations);
        when(facebook.eventOperations()).thenReturn(eventOperations);
        when(facebook.feedOperations()).thenReturn(feedOperations);
        when(facebook.commentOperations()).thenReturn(commentOperations);
    }

    @Test
    public void redirectsToLoginPageForNotLoggedInUserOnAllEventsPage() {
        when(connectionRepository.findPrimaryConnection(Facebook.class)).thenReturn(null);
        String view = controller.allEvents(model);
        assertEquals("redirect:/login", view);
    }

    @Test
    public void addsAllEventsToModel() {
        PagedList created = mock(PagedList.class);
        when(eventOperations.getCreated()).thenReturn(created);
        PagedList attending = mock(PagedList.class);
        when(eventOperations.getAttending()).thenReturn(attending);
        PagedList maybeAttending = mock(PagedList.class);
        when(eventOperations.getMaybeAttending()).thenReturn(maybeAttending);
        PagedList noReplies = mock(PagedList.class);
        when(eventOperations.getNoReplies()).thenReturn(noReplies);
        PagedList declined = mock(PagedList.class);
        when(eventOperations.getDeclined()).thenReturn(declined);

        String view = controller.allEvents(model);

        assertEquals("Tom", model.asMap().get("username"));
        Map<String, PagedList<Invitation>> invitations = (Map<String, PagedList<Invitation>>) model.asMap().get("eventGroups");
        assertEquals("home", view);
        assertEquals(created, invitations.get("1. Created events"));
        assertEquals(attending, invitations.get("2. Attending events"));
        assertEquals(maybeAttending, invitations.get("3. Maybe attending events"));
        assertEquals(noReplies, invitations.get("4. No replies events"));
        assertEquals(declined, invitations.get("5. Declined events"));
    }

    @Test
    public void redirectsToLoginPageForNotLoggedInUserOnEventPage() throws IOException {
        when(connectionRepository.findPrimaryConnection(Facebook.class)).thenReturn(null);
        String view = controller.eventData(model, "276188812727448");
        assertEquals("redirect:/login", view);
    }

    @Test
    public void addsEventToModel() throws IOException {
        Event event = mock(Event.class);
        PowerMockito.mockStatic(FbGraphApi.class);
        when(FbGraphApi.getEvent("276188812727448", connectionRepository)).thenReturn(event);
        Post post = mock(Post.class);
        when(post.getMessage()).thenReturn("message");
        when(post.getId()).thenReturn("789456123");
        PagedList<Post> eventFeed = new PagedList<Post>(singletonList(post), mock(PagingParameters.class), mock(PagingParameters.class));
        when(feedOperations.getFeed("276188812727448")).thenReturn(eventFeed);
        PagedList comments = mock(PagedList.class);
        when(commentOperations.getComments("789456123")).thenReturn(comments);

        String view = controller.eventData(model, "276188812727448");
        List<FbPost> posts = (List<FbPost>) model.asMap().get("eventPosts");

        assertEquals("Tom", model.asMap().get("username"));
        assertEquals(event, model.asMap().get("event"));
        assertEquals("event", view);
        assertEquals(1, posts.size());
        assertEquals("message", posts.get(0).getPost().getMessage());
        assertEquals(comments, posts.get(0).getComments());
    }

    @Test
    public void redirectsToLoginPageForNotLoggedInAddingMessage() throws IOException {
        when(connectionRepository.findPrimaryConnection(Facebook.class)).thenReturn(null);
        String view = controller.addMessage(model, mock(NewMessage.class));
        assertEquals("redirect:/login", view);
    }

    @Test
    public void addsMessage() throws IOException {
        NewMessage newMessage = mock(NewMessage.class);
        when(newMessage.getMsgId()).thenReturn("123456");
        when(newMessage.getContent()).thenReturn("Message content");
        when(feedOperations.post(anyString(), anyString())).thenReturn(StringUtils.EMPTY);

        String view = controller.addMessage(model, newMessage);

        assertEquals("redirect:/events/123456", view);
    }

    @Test
    public void redirectsToLoginPageForNotLoggedInAddingComment() throws IOException {
        when(connectionRepository.findPrimaryConnection(Facebook.class)).thenReturn(null);
        String view = controller.addComment(model, mock(NewMessage.class));
        assertEquals("redirect:/login", view);
    }

    @Test
    public void addsComment() throws IOException {
        NewMessage newMessage = mock(NewMessage.class);
        when(newMessage.getMsgId()).thenReturn("123456_987654");
        when(newMessage.getContent()).thenReturn("Comment msg");
        when(commentOperations.addComment(anyString(), anyString())).thenReturn(StringUtils.EMPTY);

        String view = controller.addComment(model, newMessage);

        assertEquals("redirect:/events/123456", view);
    }
}