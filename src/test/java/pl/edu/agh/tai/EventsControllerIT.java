package pl.edu.agh.tai;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.*;
import org.springframework.ui.Model;
import pl.edu.agh.tai.controllers.EventsController;
import pl.edu.agh.tai.utils.FbGraphApi;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest({ FbGraphApi.class })
public class EventsControllerIT {

    EventsController eventsController;
    Facebook facebook;
    Connection connection;
    ConnectionRepository connectionRepository;
    Model model;
    UserOperations userOperations;
    FeedOperations feedOperations;
    User user;
    Event event;
    FbGraphApi fbGraphApi;
    PagedList<Post> postList;
    Post post;

    @Before
    public void setUp() throws Exception{
        model=mock(Model.class);
        this.connectionRepository= mock(ConnectionRepository.class);
        this.connection = mock(Connection.class);

        this.facebook= mock(Facebook.class);
        this.userOperations = mock(UserOperations.class);
        this.user=mock(User.class);
        this.event=mock(Event.class);
        this.eventsController = new EventsController(facebook,connectionRepository);

        this.connectionRepository.addConnection(connection);
        this.fbGraphApi = mock(FbGraphApi.class);
        this.feedOperations = mock(FeedOperations.class);

        Post post1 = new Post();
        Post post2 = new Post();
        Post post3 = new Post();
        List<Post> list = mock(List.class);
        PagingParameters p = mock(PagingParameters.class);
        list.add(post1);
        postList = mock(PagedList.class);
        postList.add(post1);
        postList.add(post2);
        this.post=mock(Post.class);

    }

//    @Test
//    public void addCommentTest() throws Exception{
////        NewMessage msg = mock(NewMessage.class);
//
////        when(facebook.commentOperations().addComment(msg.getMsgId(),msg.getContent())).thenReturn("aaaaa");
//
//        when(connectionRepository.findPrimaryConnection(Facebook.class)).thenReturn(this.connection);
//
//        assertNotNull(eventsController);
//        assertNotNull(model);
//        assertNotNull(msg);
//        assertNotNull(msg.getMsgId());
//        assertNotNull(msg.getContent());
//        assertNotNull(connectionRepository.findPrimaryConnection(Facebook.class));
////        assertNull(eventsController.addComment(model,msg));
//
//
////        when(facebook.commentOperations().addComment(msg.getMsgId(), msg.getContent())).thenReturn("aa");
//
//        when(eventsController.addComment(model,msg)).thenReturn("aa");
//
//        assertNotNull(eventsController.addComment(model,msg));
//
//    }


    @Test
    public void eventDataTest() throws Exception{
        PowerMockito.mockStatic(FbGraphApi.class);

        when(connectionRepository.findPrimaryConnection(Facebook.class)).thenReturn(this.connection);
        when(facebook.userOperations()).thenReturn(userOperations);
        when(facebook.userOperations().getUserProfile()).thenReturn(this.user);
        when(facebook.userOperations().getUserProfile().getName()).thenReturn("aa");
        when(FbGraphApi.getAccessToken(connectionRepository)).thenReturn("EAACEdEose0cBAGbvwFLlPhRJ1XoZBN9zshGf511lZARZC4w4ygsM9VDpXmogYMQ8U0yz6xsFrO5nHfVqvGopOVZByxUiAjziLtDfwIStXiZBtzZBjFmIG54aGHJd4HvvOuOhvkcKtlJBdaANeaAAiILc8pkFngE6iXPrD8xZAgy7wZDZD");
        when(FbGraphApi.getEvent("276188812727448",connectionRepository)).thenReturn(this.event);
        assertNotNull(connectionRepository);
        assertNotNull(facebook.userOperations().getUserProfile().getName());
        assertNotNull(model);

        List<Post> _list = new ArrayList<>();
        when(facebook.feedOperations()).thenReturn(feedOperations);
        when(facebook.feedOperations().getFeed("276188812727448")).thenReturn(new PagedList<Post>(_list,null,null,0));

        assertEquals("event",eventsController.eventData(model,"276188812727448"));



    }


}
