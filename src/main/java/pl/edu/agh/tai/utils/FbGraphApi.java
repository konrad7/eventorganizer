package pl.edu.agh.tai.utils;


import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.connect.support.OAuth2Connection;
import org.springframework.social.facebook.api.Event;
import org.springframework.social.facebook.api.Facebook;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class FbGraphApi {

    private static final String GRAPH_API_URL = "https://graph.facebook.com/v2.6/";
    private static final String ACCESS_TOKEN_KEY = "?access_token=";


    public static Event getEvent(String eventId, ConnectionRepository connectionRepository) {
        String url = GRAPH_API_URL + eventId + ACCESS_TOKEN_KEY + getAccessToken(connectionRepository);
        String result = null;
        try {
            result = getResponse(url);
        } catch (IOException e) {
            //TODO: log error
        }
        return new Gson().fromJson(result, Event.class);
    }

    public static String getAccessToken(ConnectionRepository connectionRepository) {
        OAuth2Connection con = (OAuth2Connection) connectionRepository.findPrimaryConnection(Facebook.class);
        FieldUtils.getDeclaredField(con.getClass(), "accessToken");
        String token = StringUtils.EMPTY;
        try {
            token = (String) FieldUtils.readField(con, "accessToken", true);
        } catch (IllegalAccessException e) {
            //TODO: log error
        }
        return token;
    }

    private static String getResponse(String url) throws IOException {
        URL fbUrl = new URL(url);
        HttpsURLConnection con = (HttpsURLConnection)fbUrl.openConnection();
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuilder sb = new StringBuilder();
        while ((inputLine = in.readLine()) != null)
        {
            sb.append(inputLine);
        }

        return sb.toString();
    }

}
