package pl.edu.agh.tai.controllers;

import org.apache.commons.lang3.StringUtils;
import org.springframework.social.connect.ConnectionRepository;
import org.springframework.social.facebook.api.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.agh.tai.domain.FbPost;
import pl.edu.agh.tai.domain.NewMessage;
import pl.edu.agh.tai.utils.FbGraphApi;

import javax.inject.Inject;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

@Controller
@RequestMapping("events")
public class EventsController {

    private Facebook facebook;
    private ConnectionRepository connectionRepository;

    @Inject
    public EventsController(Facebook facebook, ConnectionRepository connectionRepository) {
        this.facebook = facebook;
        this.connectionRepository = connectionRepository;
    }

    @RequestMapping(method=RequestMethod.GET)
    public String allEvents(Model model) {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/login";
        }

        model.addAttribute("username", facebook.userOperations().getUserProfile().getName());

        Map<String, PagedList<Invitation>> invitations = new TreeMap<>();
        invitations.put("1. Created events", facebook.eventOperations().getCreated());
        invitations.put("2. Attending events", facebook.eventOperations().getAttending());
        invitations.put("3. Maybe attending events", facebook.eventOperations().getMaybeAttending());
        invitations.put("4. No replies events", facebook.eventOperations().getNoReplies());
        invitations.put("5. Declined events", facebook.eventOperations().getDeclined());
        model.addAttribute("eventGroups", invitations);

        return "home";
    }

    @RequestMapping(value="/{eventId}")
    public String eventData(Model model, @PathVariable String eventId) throws IOException {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/login";
        }
        model.addAttribute("newMsg", new NewMessage());
        model.addAttribute("username", facebook.userOperations().getUserProfile().getName());

        Event event = FbGraphApi.getEvent(eventId, connectionRepository);

        model.addAttribute("event", event);

        PagedList<Post> feeds = facebook.feedOperations().getFeed(eventId);

        List<FbPost> posts = new ArrayList<>();

        for (Post p : feeds) {
            if (StringUtils.isNotBlank(p.getMessage())) {
                FbPost post = new FbPost();
                post.setPost(p);
                PagedList<Comment> comments = facebook.commentOperations().getComments(p.getId());
                post.setComments(comments);
                posts.add(post);
            }
        }

        model.addAttribute("eventPosts", posts);

        return "event";
    }

    @RequestMapping(value="/message/new", method = RequestMethod.POST)
    public String addMessage(Model model, @ModelAttribute NewMessage msg) throws IOException {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/login";
        }

        facebook.feedOperations().post(msg.getMsgId(), msg.getContent());
        return "redirect:/events/" + msg.getMsgId();
    }

    @RequestMapping(value="/comment/new", method = RequestMethod.POST)
    public String addComment(Model model, @ModelAttribute NewMessage msg) throws IOException {
        if (connectionRepository.findPrimaryConnection(Facebook.class) == null) {
            return "redirect:/login";
        }

        facebook.commentOperations().addComment(msg.getMsgId(), msg.getContent());
        String eventId = msg.getMsgId().split("_")[0];
        return "redirect:/events/" + eventId;
    }
}