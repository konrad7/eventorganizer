package pl.edu.agh.tai.domain;

import org.springframework.social.facebook.api.Comment;
import org.springframework.social.facebook.api.PagedList;
import org.springframework.social.facebook.api.Post;

public class FbPost {

    private Post post;
    private PagedList<Comment> comments;

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public PagedList<Comment> getComments() {
        return comments;
    }

    public void setComments(PagedList<Comment> comments) {
        this.comments = comments;
    }
}
