package pl.edu.agh.tai.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
public class MvcConfig extends WebMvcConfigurerAdapter {

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/events").setViewName("home");
        registry.addViewController("/login").setViewName("login");

        registry.addRedirectViewController("/", "/events");
        registry.addRedirectViewController("/authenticate/facebook", "/auth/facebook?scope=email,user_events,rsvp_event,publish_actions");
    }

}